package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.display.Fl1DisplayItem;

public class Fl1DisplayModel extends GeoModel<Fl1DisplayItem> {
	@Override
	public ResourceLocation getAnimationResource(Fl1DisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "animations/fructgeckolib_n.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Fl1DisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "geo/fructgeckolib_n.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Fl1DisplayItem entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/voaushutshjjjm.png");
	}
}
