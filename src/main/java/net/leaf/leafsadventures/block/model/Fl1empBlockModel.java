package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.entity.Fl1empTileEntity;

public class Fl1empBlockModel extends GeoModel<Fl1empTileEntity> {
	@Override
	public ResourceLocation getAnimationResource(Fl1empTileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "animations/fructgeckolib_n_emp.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Fl1empTileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "geo/fructgeckolib_n_emp.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Fl1empTileEntity entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/voaushutshjjjm.png");
	}
}
