package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.entity.Fl1stage2TileEntity;

public class Fl1stage2BlockModel extends GeoModel<Fl1stage2TileEntity> {
	@Override
	public ResourceLocation getAnimationResource(Fl1stage2TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "animations/fructgeckolib_2n.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Fl1stage2TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "geo/fructgeckolib_2n.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Fl1stage2TileEntity entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/voaushutllshssssssm1.png");
	}
}
