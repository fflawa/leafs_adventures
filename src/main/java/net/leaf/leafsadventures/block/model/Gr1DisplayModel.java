package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.display.Gr1DisplayItem;

public class Gr1DisplayModel extends GeoModel<Gr1DisplayItem> {
	@Override
	public ResourceLocation getAnimationResource(Gr1DisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "animations/gr1.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Gr1DisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "geo/gr1.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Gr1DisplayItem entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/gr1.png");
	}
}
