package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.entity.Gr1TileEntity;

public class Gr1BlockModel extends GeoModel<Gr1TileEntity> {
	@Override
	public ResourceLocation getAnimationResource(Gr1TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "animations/gr1.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Gr1TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "geo/gr1.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Gr1TileEntity entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/gr1.png");
	}
}
