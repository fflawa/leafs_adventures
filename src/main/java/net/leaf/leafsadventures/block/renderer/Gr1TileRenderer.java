package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoBlockRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Gr1BlockModel;
import net.leaf.leafsadventures.block.entity.Gr1TileEntity;

public class Gr1TileRenderer extends GeoBlockRenderer<Gr1TileEntity> {
	public Gr1TileRenderer() {
		super(new Gr1BlockModel());
	}

	@Override
	public RenderType getRenderType(Gr1TileEntity animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
