package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoItemRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Fl1DisplayModel;
import net.leaf.leafsadventures.block.display.Fl1DisplayItem;

public class Fl1DisplayItemRenderer extends GeoItemRenderer<Fl1DisplayItem> {
	public Fl1DisplayItemRenderer() {
		super(new Fl1DisplayModel());
	}

	@Override
	public RenderType getRenderType(Fl1DisplayItem animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
