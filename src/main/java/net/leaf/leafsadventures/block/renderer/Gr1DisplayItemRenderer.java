package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoItemRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Gr1DisplayModel;
import net.leaf.leafsadventures.block.display.Gr1DisplayItem;

public class Gr1DisplayItemRenderer extends GeoItemRenderer<Gr1DisplayItem> {
	public Gr1DisplayItemRenderer() {
		super(new Gr1DisplayModel());
	}

	@Override
	public RenderType getRenderType(Gr1DisplayItem animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
