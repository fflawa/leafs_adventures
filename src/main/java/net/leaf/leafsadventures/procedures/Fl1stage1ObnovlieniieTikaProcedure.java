package net.leaf.leafsadventures.procedures;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.GameRules;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.BlockPos;

import net.leaf.leafsadventures.init.LeafsAdventuresModBlocks;
import net.leaf.leafsadventures.LeafsAdventuresMod;

public class Fl1stage1ObnovlieniieTikaProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z) {
		LeafsAdventuresMod.queueServerWork((int) ((6000 * 3) / (world.getLevelData().getGameRules().getInt(GameRules.RULE_RANDOMTICKING))), () -> {
			if (world instanceof ServerLevel _level)
				_level.sendParticles(ParticleTypes.HAPPY_VILLAGER, x, y, z, 5, 3, 3, 3, 1);
			world.setBlock(BlockPos.containing(x, y, z), LeafsAdventuresModBlocks.FL_1STAGE_2.get().defaultBlockState(), 3);
		});
	}
}
