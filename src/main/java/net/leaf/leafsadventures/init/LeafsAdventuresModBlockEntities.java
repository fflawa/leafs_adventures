
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.leaf.leafsadventures.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.Block;

import net.leaf.leafsadventures.block.entity.Gr1TileEntity;
import net.leaf.leafsadventures.block.entity.Fl1stage2TileEntity;
import net.leaf.leafsadventures.block.entity.Fl1stage1TileEntity;
import net.leaf.leafsadventures.block.entity.Fl1empTileEntity;
import net.leaf.leafsadventures.block.entity.Fl1TileEntity;
import net.leaf.leafsadventures.LeafsAdventuresMod;

public class LeafsAdventuresModBlockEntities {
	public static final DeferredRegister<BlockEntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, LeafsAdventuresMod.MODID);
	public static final RegistryObject<BlockEntityType<Fl1TileEntity>> FL_1 = REGISTRY.register("fl_1", () -> BlockEntityType.Builder.of(Fl1TileEntity::new, LeafsAdventuresModBlocks.FL_1.get()).build(null));
	public static final RegistryObject<BlockEntityType<Fl1stage2TileEntity>> FL_1STAGE_2 = REGISTRY.register("fl_1stage_2", () -> BlockEntityType.Builder.of(Fl1stage2TileEntity::new, LeafsAdventuresModBlocks.FL_1STAGE_2.get()).build(null));
	public static final RegistryObject<BlockEntityType<Fl1stage1TileEntity>> FL_1STAGE_1 = REGISTRY.register("fl_1stage_1", () -> BlockEntityType.Builder.of(Fl1stage1TileEntity::new, LeafsAdventuresModBlocks.FL_1STAGE_1.get()).build(null));
	public static final RegistryObject<BlockEntityType<Fl1empTileEntity>> FL_1EMP = REGISTRY.register("fl_1emp", () -> BlockEntityType.Builder.of(Fl1empTileEntity::new, LeafsAdventuresModBlocks.FL_1EMP.get()).build(null));
	public static final RegistryObject<BlockEntityType<Gr1TileEntity>> GR_1 = REGISTRY.register("gr_1", () -> BlockEntityType.Builder.of(Gr1TileEntity::new, LeafsAdventuresModBlocks.GR_1.get()).build(null));

	private static RegistryObject<BlockEntityType<?>> register(String registryname, RegistryObject<Block> block, BlockEntityType.BlockEntitySupplier<?> supplier) {
		return REGISTRY.register(registryname, () -> BlockEntityType.Builder.of(supplier, block.get()).build(null));
	}
}
