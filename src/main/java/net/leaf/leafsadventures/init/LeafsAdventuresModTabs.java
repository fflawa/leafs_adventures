
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.leaf.leafsadventures.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.network.chat.Component;
import net.minecraft.core.registries.Registries;

import net.leaf.leafsadventures.LeafsAdventuresMod;

public class LeafsAdventuresModTabs {
	public static final DeferredRegister<CreativeModeTab> REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, LeafsAdventuresMod.MODID);
	public static final RegistryObject<CreativeModeTab> LEAFSADVENTURES = REGISTRY.register("leafsadventures",
			() -> CreativeModeTab.builder().title(Component.translatable("item_group.leafs_adventures.leafsadventures")).icon(() -> new ItemStack(LeafsAdventuresModBlocks.FL_1.get())).displayItems((parameters, tabData) -> {
				tabData.accept(LeafsAdventuresModBlocks.FL_1.get().asItem());
				tabData.accept(LeafsAdventuresModBlocks.FL_1STAGE_2.get().asItem());
				tabData.accept(LeafsAdventuresModBlocks.FL_1STAGE_1.get().asItem());
				tabData.accept(LeafsAdventuresModBlocks.FL_1EMP.get().asItem());
				tabData.accept(LeafsAdventuresModItems.FLFR.get());
				tabData.accept(LeafsAdventuresModBlocks.SILT.get().asItem());
				tabData.accept(LeafsAdventuresModItems.FL_1SEEDS.get());
				tabData.accept(LeafsAdventuresModBlocks.GR_1.get().asItem());
			})

					.build());
}
