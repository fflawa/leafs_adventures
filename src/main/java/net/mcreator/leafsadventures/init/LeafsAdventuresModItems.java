
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.leaf.leafsadventures.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.item.Item;

import net.leaf.leafsadventures.item.FlfrItem;
import net.leaf.leafsadventures.block.display.Fl1stage2DisplayItem;
import net.leaf.leafsadventures.block.display.Fl1stage1DisplayItem;
import net.leaf.leafsadventures.block.display.Fl1empDisplayItem;
import net.leaf.leafsadventures.block.display.Fl1DisplayItem;
import net.leaf.leafsadventures.LeafsAdventuresMod;

public class LeafsAdventuresModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, LeafsAdventuresMod.MODID);
	public static final RegistryObject<Item> FL_1 = REGISTRY.register(LeafsAdventuresModBlocks.FL_1.getId().getPath(), () -> new Fl1DisplayItem(LeafsAdventuresModBlocks.FL_1.get(), new Item.Properties()));
	public static final RegistryObject<Item> FL_1STAGE_2 = REGISTRY.register(LeafsAdventuresModBlocks.FL_1STAGE_2.getId().getPath(), () -> new Fl1stage2DisplayItem(LeafsAdventuresModBlocks.FL_1STAGE_2.get(), new Item.Properties()));
	public static final RegistryObject<Item> FL_1STAGE_1 = REGISTRY.register(LeafsAdventuresModBlocks.FL_1STAGE_1.getId().getPath(), () -> new Fl1stage1DisplayItem(LeafsAdventuresModBlocks.FL_1STAGE_1.get(), new Item.Properties()));
	public static final RegistryObject<Item> FL_1EMP = REGISTRY.register(LeafsAdventuresModBlocks.FL_1EMP.getId().getPath(), () -> new Fl1empDisplayItem(LeafsAdventuresModBlocks.FL_1EMP.get(), new Item.Properties()));
	public static final RegistryObject<Item> FLFR = REGISTRY.register("flfr", () -> new FlfrItem());
}
