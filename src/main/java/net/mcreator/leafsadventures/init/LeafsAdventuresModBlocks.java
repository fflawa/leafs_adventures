
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.leaf.leafsadventures.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import net.leaf.leafsadventures.block.Fl1stage2Block;
import net.leaf.leafsadventures.block.Fl1stage1Block;
import net.leaf.leafsadventures.block.Fl1empBlock;
import net.leaf.leafsadventures.block.Fl1Block;
import net.leaf.leafsadventures.LeafsAdventuresMod;

public class LeafsAdventuresModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, LeafsAdventuresMod.MODID);
	public static final RegistryObject<Block> FL_1 = REGISTRY.register("fl_1", () -> new Fl1Block());
	public static final RegistryObject<Block> FL_1STAGE_2 = REGISTRY.register("fl_1stage_2", () -> new Fl1stage2Block());
	public static final RegistryObject<Block> FL_1STAGE_1 = REGISTRY.register("fl_1stage_1", () -> new Fl1stage1Block());
	public static final RegistryObject<Block> FL_1EMP = REGISTRY.register("fl_1emp", () -> new Fl1empBlock());
}
