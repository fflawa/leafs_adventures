package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoBlockRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Fl1stage2BlockModel;
import net.leaf.leafsadventures.block.entity.Fl1stage2TileEntity;

public class Fl1stage2TileRenderer extends GeoBlockRenderer<Fl1stage2TileEntity> {
	public Fl1stage2TileRenderer() {
		super(new Fl1stage2BlockModel());
	}

	@Override
	public RenderType getRenderType(Fl1stage2TileEntity animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
