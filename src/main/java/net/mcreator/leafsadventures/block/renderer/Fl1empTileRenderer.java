package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoBlockRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Fl1empBlockModel;
import net.leaf.leafsadventures.block.entity.Fl1empTileEntity;

public class Fl1empTileRenderer extends GeoBlockRenderer<Fl1empTileEntity> {
	public Fl1empTileRenderer() {
		super(new Fl1empBlockModel());
	}

	@Override
	public RenderType getRenderType(Fl1empTileEntity animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
