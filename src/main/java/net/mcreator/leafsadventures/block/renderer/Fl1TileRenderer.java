package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoBlockRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Fl1BlockModel;
import net.leaf.leafsadventures.block.entity.Fl1TileEntity;

public class Fl1TileRenderer extends GeoBlockRenderer<Fl1TileEntity> {
	public Fl1TileRenderer() {
		super(new Fl1BlockModel());
	}

	@Override
	public RenderType getRenderType(Fl1TileEntity animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
