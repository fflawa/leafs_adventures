package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoItemRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Fl1stage2DisplayModel;
import net.leaf.leafsadventures.block.display.Fl1stage2DisplayItem;

public class Fl1stage2DisplayItemRenderer extends GeoItemRenderer<Fl1stage2DisplayItem> {
	public Fl1stage2DisplayItemRenderer() {
		super(new Fl1stage2DisplayModel());
	}

	@Override
	public RenderType getRenderType(Fl1stage2DisplayItem animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
