package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoItemRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Fl1empDisplayModel;
import net.leaf.leafsadventures.block.display.Fl1empDisplayItem;

public class Fl1empDisplayItemRenderer extends GeoItemRenderer<Fl1empDisplayItem> {
	public Fl1empDisplayItemRenderer() {
		super(new Fl1empDisplayModel());
	}

	@Override
	public RenderType getRenderType(Fl1empDisplayItem animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
