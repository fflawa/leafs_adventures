package net.leaf.leafsadventures.block.renderer;

import software.bernie.geckolib.renderer.GeoBlockRenderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.MultiBufferSource;

import net.leaf.leafsadventures.block.model.Fl1stage1BlockModel;
import net.leaf.leafsadventures.block.entity.Fl1stage1TileEntity;

public class Fl1stage1TileRenderer extends GeoBlockRenderer<Fl1stage1TileEntity> {
	public Fl1stage1TileRenderer() {
		super(new Fl1stage1BlockModel());
	}

	@Override
	public RenderType getRenderType(Fl1stage1TileEntity animatable, ResourceLocation texture, MultiBufferSource bufferSource, float partialTick) {
		return RenderType.entityTranslucent(getTextureLocation(animatable));
	}
}
