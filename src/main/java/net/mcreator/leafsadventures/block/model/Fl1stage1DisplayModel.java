package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.display.Fl1stage1DisplayItem;

public class Fl1stage1DisplayModel extends GeoModel<Fl1stage1DisplayItem> {
	@Override
	public ResourceLocation getAnimationResource(Fl1stage1DisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "animations/stage1.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Fl1stage1DisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "geo/stage1.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Fl1stage1DisplayItem entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/fffff.png");
	}
}
