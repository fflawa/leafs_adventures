package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.entity.Fl1stage1TileEntity;

public class Fl1stage1BlockModel extends GeoModel<Fl1stage1TileEntity> {
	@Override
	public ResourceLocation getAnimationResource(Fl1stage1TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "animations/stage1.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Fl1stage1TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "geo/stage1.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Fl1stage1TileEntity entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/fffff.png");
	}
}
