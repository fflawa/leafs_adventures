package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.display.Fl1empDisplayItem;

public class Fl1empDisplayModel extends GeoModel<Fl1empDisplayItem> {
	@Override
	public ResourceLocation getAnimationResource(Fl1empDisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "animations/fructgeckolibempty.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Fl1empDisplayItem animatable) {
		return new ResourceLocation("leafs_adventures", "geo/fructgeckolibempty.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Fl1empDisplayItem entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/voaushutshm.png");
	}
}
