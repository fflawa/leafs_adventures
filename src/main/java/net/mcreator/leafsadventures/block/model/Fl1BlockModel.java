package net.leaf.leafsadventures.block.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.leaf.leafsadventures.block.entity.Fl1TileEntity;

public class Fl1BlockModel extends GeoModel<Fl1TileEntity> {
	@Override
	public ResourceLocation getAnimationResource(Fl1TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "animations/fructgeckolib.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(Fl1TileEntity animatable) {
		return new ResourceLocation("leafs_adventures", "geo/fructgeckolib.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(Fl1TileEntity entity) {
		return new ResourceLocation("leafs_adventures", "textures/block/voaushutshm.png");
	}
}
