package net.leaf.leafsadventures.block.listener;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.leaf.leafsadventures.init.LeafsAdventuresModBlockEntities;
import net.leaf.leafsadventures.block.renderer.Fl1stage2TileRenderer;
import net.leaf.leafsadventures.block.renderer.Fl1stage1TileRenderer;
import net.leaf.leafsadventures.block.renderer.Fl1empTileRenderer;
import net.leaf.leafsadventures.block.renderer.Fl1TileRenderer;
import net.leaf.leafsadventures.LeafsAdventuresMod;

@Mod.EventBusSubscriber(modid = LeafsAdventuresMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientListener {
	@OnlyIn(Dist.CLIENT)
	@SubscribeEvent
	public static void registerRenderers(EntityRenderersEvent.RegisterRenderers event) {
		event.registerBlockEntityRenderer(LeafsAdventuresModBlockEntities.FL_1.get(), context -> new Fl1TileRenderer());
		event.registerBlockEntityRenderer(LeafsAdventuresModBlockEntities.FL_1STAGE_2.get(), context -> new Fl1stage2TileRenderer());
		event.registerBlockEntityRenderer(LeafsAdventuresModBlockEntities.FL_1STAGE_1.get(), context -> new Fl1stage1TileRenderer());
		event.registerBlockEntityRenderer(LeafsAdventuresModBlockEntities.FL_1EMP.get(), context -> new Fl1empTileRenderer());
	}
}
